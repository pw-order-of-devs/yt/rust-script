#!/usr/bin/env rust-script

//! how to install:
//! cargo install rust-script
//! Rust 1.47 +
//!
//! optionally: add the directory with cargo script to PATH
//! ~/.cargo/bin
//!
//! to run it, use: rust-script script.rs
//!
//! env:
//! RUST_SCRIPT_BASE_PATH
//! RUST_SCRIPT_PKG_NAME
//! RUST_SCRIPT_SAFE_NAME
//! RUST_SCRIPT_PATH
//!
//! other examples:
//! rust-script -e '1+2'
//! rust-script --dep time=0.1.38 -e "time::now().rfc822z().to_string()"
//! cat file.txt | rust-script --count --loop "|l, n| println!(\"{:>6}: {}\", n, l.trim_right())"
//!
//! ```cargo
//! [dependencies]
//! time = "0.1"
//! ```

use time;

macro_rules! current_time {
    () => {
        println!("{}", time::now().rfc822z());
    };
}

fn current_time() {
    println!("{}", time::now().rfc822z());
}

fn main() {
    println!("Hello world!");
    current_time();
    current_time!();
}
